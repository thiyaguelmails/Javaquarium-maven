package com.javaquarium.beans.data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * JavaBean to represent a PoissonDO in an aquarium
 * 
 * @author Valentin
 *
 */

@Entity
@Table(name = "t_aquarium", uniqueConstraints = { @UniqueConstraint(columnNames = { "c_userId", "c_especeId" }) })
public class AquariumPoissonDO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "c_id")
	private int id;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "c_userId")
	private UserDO user;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "c_especeId")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private PoissonDO espece;

	@Column(name = "c_quantity")
	private int quantity;

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public UserDO getUser() {
		return this.user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(final UserDO user) {
		this.user = user;
	}

	/**
	 * @return the pDo
	 */
	public PoissonDO getEspece() {
		return this.espece;
	}

	/**
	 * @param pDo
	 *            the pDo to set
	 */
	public void setEspece(final PoissonDO pDo) {
		this.espece = pDo;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(final int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Type : " + getClass().getName() + "\n" + "Id : " + this.id + "\n" + "User : " + this.user.toString()
				+ "\n" + "Espece : " + this.espece.toString() + "\n" + "Quantit� : " + this.quantity + "\n";
	}

}
