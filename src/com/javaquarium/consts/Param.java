package com.javaquarium.consts;

/**
 * A final class to store the diferent parameters names used in URL (useful to
 * synchronise code between jsp and actions)
 * 
 * @author Valentin
 *
 */
public final class Param {

	/**
	 * The param used when editing a PoissonVO
	 */
	public static final String ESPECE_ID = "especeId";

	/**
	 * The param used when editing a UserVO
	 */
	public static final String USER_ID = "userId";

	/**
	 * The param used to change language when clicking on different flags
	 */
	public static final String LOCALE_NAME = "localeName";

	/**
	 * The param used to give a specific redirect path (dynamic Action Forward)
	 */
	public static final String REDIRECT = "redirect";

	/**
	 * The param used to know if "not_logged" message must be shown
	 */
	public static final String MESSAGE = "message";

	/**
	 * The param used to define the page number for large results display (ex : with
	 * a table)
	 */
	public static final String PAGE = "page";

	/**
	 * DON'T CALL ME !!!
	 */
	private Param() {
	}

}
