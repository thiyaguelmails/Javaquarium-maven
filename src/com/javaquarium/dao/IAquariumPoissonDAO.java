package com.javaquarium.dao;

import java.util.List;

import com.javaquarium.beans.data.AquariumPoissonDO;
import com.javaquarium.exception.DAOException;

/**
 * Classic DAO interface to interact with AquariumPoisson object
 * 
 * @author Valentin
 *
 */
public interface IAquariumPoissonDAO {

	/**
	 * Add a new AquariumPoisson to the database
	 * 
	 * @param aPdo
	 *            The aquariumPoissonDO to add
	 * @throws DAOException
	 *             In case of errors
	 */
	void addNewAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException;

	/**
	 * Update an existing aquariumPoisson into the database
	 * 
	 * @param aPdo
	 *            The aquariumPoissonDO to update
	 * @throws DAOException
	 *             In case of errors
	 */
	void updateAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException;

	/**
	 * Removes a specific aquariumPoissonDO from the database
	 * 
	 * @param aPdo
	 *            The aquariumPoissonDO to remove
	 * @throws DAOException
	 *             In case of errors
	 */
	void removeAquariumPoisson(final AquariumPoissonDO aPdo) throws DAOException;

	/**
	 * Removes all aquariumPoissonDO of a specific user
	 * 
	 * @param userId
	 *            The user specificed
	 * @throws DAOException
	 *             In case of errors
	 */
	void removeAllAquariumPoisson(final int userId) throws DAOException;

	/**
	 * Get all aquariumPoisson of a specific user
	 * 
	 * @param userId
	 *            The user specified
	 * @return A list of all aquariumPoisson of this user
	 * @throws DAOException
	 *             In case of errors
	 */
	List<AquariumPoissonDO> getAllAquariumPoisson(final int userId) throws DAOException;

	/**
	 * Get all aquariumPoisson of a specific espece in all user aquarium
	 * 
	 * @param especeId
	 *            The especeId of the aquariumPoisson
	 * @return A list of all aquariumPoisson of this espece in all existing aquarium
	 * @throws DAOException
	 *             In case of errors
	 */
	List<AquariumPoissonDO> getAllAquariumPoissonByUser(final int especeId) throws DAOException;

	/**
	 * Get a specific aquariumPoisson from the user's aquarium
	 * 
	 * @param userId
	 *            The user specified
	 * @param especeId
	 *            The espece of the aquariumPoisson searched
	 * @return The aquariumPoissonDO matching with the two parameters
	 * @throws DAOException
	 *             In case of errors
	 */
	AquariumPoissonDO getAquariumPoisson(final int userId, final int especeId) throws DAOException;

	/**
	 * Get the total of aquariumPoisson into the user's aquarium
	 * 
	 * @param userId
	 *            The user specified
	 * @return The total number of aquariumPoisson in his aquarium
	 * @throws DAOException
	 *             In case of errors
	 */
	int getTotalAquariumPoissons(final int userId) throws DAOException;

}
