package com.javaquarium.dao;

import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;

import com.javaquarium.beans.data.UserDO;
import com.javaquarium.consts.ExceptionCode;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.exception.DAOException;
import com.javaquarium.util.HibernateUtils;

/**
 * Classic DAO class to manage UserDO and interact with the database
 * 
 * @author Valentin
 *
 */
public class UserDAO implements IUserDAO {

	/**
	 * The parameter name for the userId
	 */
	public static final String PARAM_USERID = "id";
	/**
	 * The parameter name for the userName
	 */
	public static final String PARAM_USERNAME = "username";
	/**
	 * The parameter name for the userPassword
	 */
	public static final String PARAM_USERPASS = "password";

	@Override
	public UserDO getOne(final String username, final String password) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session
				.createQuery("from UserDO where username= :" + PARAM_USERNAME + " and password= :" + PARAM_USERPASS);
		query.setParameter(PARAM_USERNAME, username);
		query.setParameter(PARAM_USERPASS, password);

		try {
			return (UserDO) query.uniqueResult();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final NonUniqueResultException nre) {
			throw new DAOException(ExceptionCode.ERROR_GET_NONUNIQUE, MessageKey.ERROR_GET_NONUNIQUE,
					nre.getMessage().toString());
		} finally {
			session.close();
		}
	}

	@Override
	public UserDO getOneById(final int userId) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Query query = session.createQuery("from UserDO where id= :" + PARAM_USERID);
		query.setParameter(PARAM_USERID, userId);
		try {
			return (UserDO) query.uniqueResult();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final NonUniqueResultException nre) {
			throw new DAOException(ExceptionCode.ERROR_GET_NONUNIQUE, MessageKey.ERROR_GET_NONUNIQUE,
					nre.getMessage().toString());
		} finally {
			session.close();
		}

	}

	@Override
	public void addOne(final UserDO uDo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.save(uDo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final ConstraintViolationException cve) {
			throw new DAOException(ExceptionCode.ERROR_DUPLICATE_USER, MessageKey.ERROR_DUPLICATE_USER, uDo);
		} finally {
			session.close();
		}

	}

	@Override
	public void update(final UserDO uDo) throws DAOException {
		final Session session = HibernateUtils.getSession();
		final Transaction tx = session.beginTransaction();
		try {
			session.update(uDo);
			tx.commit();
		} catch (final JDBCConnectionException ce) {
			throw new DAOException(ExceptionCode.ERROR_DATABASE_CONNECTION, MessageKey.ERROR_DATABASE_CONNECTION);
		} catch (final ConstraintViolationException cve) {
			throw new DAOException(ExceptionCode.ERROR_DUPLICATE_USER, MessageKey.ERROR_DUPLICATE_USER, uDo);
		} finally {
			session.close();
		}
	}

}
