package com.javaquarium.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.javaquarium.business.IAquariumPoissonService;
import com.javaquarium.business.IPoissonService;
import com.javaquarium.business.IUserService;

/**
 * Class used as a catalogue for all services It will return an instance
 * (created by Spring) of the service requested
 * 
 * @author Valentin
 *
 */
public class CatalogueService {

	/**
	 * Instance of the PoissonService
	 */
	private IPoissonService poissonService;

	/**
	 * Instance of the userService
	 */
	private IUserService userService;

	/**
	 * Instance of the aquariumPoissonService
	 */
	private IAquariumPoissonService aquariumPoissonService;

	/**
	 * The logger object to log info/messages
	 */
	private final Logger logger;

	public CatalogueService() {
		logger = Logger.getLogger(this.getClass().getName());
	}

	/**
	 * @return the poissonService
	 */
	public IPoissonService getPoissonService() {
		logger.log(Level.INFO, "Le Service Poisson est demandé");
		return this.poissonService;
	}

	/**
	 * @param poissonService
	 *            the poissonService to set
	 */
	public void setPoissonService(final IPoissonService poissonService) {
		this.poissonService = poissonService;
	}

	/**
	 * @return the loginService
	 */
	public IUserService getUserService() {
		logger.log(Level.INFO, "Le Service User est demandé");
		return this.userService;
	}

	/**
	 * @param loginService
	 *            the loginService to set
	 */
	public void setUserService(final IUserService loginService) {
		this.userService = loginService;
	}

	/**
	 * @return the aquariumPoissonService
	 */
	public IAquariumPoissonService getAquariumPoissonService() {
		logger.log(Level.INFO, "Le Service AquariumPoisson est demandé");
		return this.aquariumPoissonService;
	}

	/**
	 * @param aquariumPoissonService
	 *            the aquariumPoissonService to set
	 */
	public void setAquariumPoissonService(final IAquariumPoissonService aquariumPoissonService) {
		this.aquariumPoissonService = aquariumPoissonService;
	}

}
