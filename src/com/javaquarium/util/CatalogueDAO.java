package com.javaquarium.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.javaquarium.dao.IAquariumPoissonDAO;
import com.javaquarium.dao.IPoissonDAO;
import com.javaquarium.dao.IUserDAO;

/**
 * Class used as a catalogue for all DAOs It will return an instance of the DAO
 * (created by Spring) requested
 * 
 * @author Valentin
 *
 */
public class CatalogueDAO {

	/**
	 * Instance of the poissonDAO
	 */
	private IPoissonDAO poissonDAO;

	/**
	 * Instance of the userDAO
	 */
	private IUserDAO userDAO;

	/**
	 * Instance of the aquariumPoissonDAO
	 */
	private IAquariumPoissonDAO aquariumPoissonDAO;

	/**
	 * The logger object to log info messages
	 */
	private final Logger logger;

	public CatalogueDAO() {
		logger = Logger.getLogger(this.getClass().getName());
	}

	/**
	 * @return the poissonDAO
	 */
	public IPoissonDAO getPoissonDAO() {
		logger.log(Level.INFO, "Le DAO Poisson est demandé");
		return this.poissonDAO;
	}

	/**
	 * @param poissonDAO
	 *            the poissonDAO to set
	 */
	public void setPoissonDAO(final IPoissonDAO poissonDAO) {
		this.poissonDAO = poissonDAO;
	}

	/**
	 * @return the loginDAO
	 */
	public IUserDAO getUserDAO() {
		logger.log(Level.INFO, "Le DAO User est demandé");
		return this.userDAO;
	}

	/**
	 * @param userDAO
	 *            the userDAO to set
	 */
	public void setUserDAO(final IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	/**
	 * @return the aquariumPoissonDAO
	 */
	public IAquariumPoissonDAO getAquariumPoissonDAO() {
		logger.log(Level.INFO, "Le DAO AquariumPoisson est demandé");
		return this.aquariumPoissonDAO;
	}

	/**
	 * @param aquariumPoissonDAO
	 *            the aquariumPoissonDAO to set
	 */
	public void setAquariumPoissonDAO(final IAquariumPoissonDAO aquariumPoissonDAO) {
		this.aquariumPoissonDAO = aquariumPoissonDAO;
	}

}
