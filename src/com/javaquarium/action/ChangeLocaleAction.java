package com.javaquarium.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;

/**
 * Action to change the locale of the website
 * 
 * @author Valentin
 *
 */
public class ChangeLocaleAction extends Action {

	/**
	 * Constant name of struts locale key to change the locale
	 */
	public static final String STRUTS_LOCALE_KEY = "org.apache.struts.action.LOCALE";

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final ActionMessages messages = new ActionMessages();
		final HttpSession session = request.getSession();
		if (request.getParameter(Param.LOCALE_NAME) != null && request.getParameter(Param.REDIRECT) != null) {
			final String locale = request.getParameter(Param.LOCALE_NAME).toString();
			final String redirect = request.getParameter(Param.REDIRECT);
			session.setAttribute(STRUTS_LOCALE_KEY, LocaleUtils.toLocale(locale));
			// Dynamic ActionForWard to stay on the same page where the button was clicked
			final ActionForward actionfor = new ActionForward();
			actionfor.setRedirect(true);
			actionfor.setPath("/" + redirect);
			return actionfor;
		} else {
			messages.add(MessageType.ERROR.toString(), new ActionMessage(MessageKey.ERROR_MISSING_PARAMETER));
			saveMessages(request, messages);
			return mapping.findForward(Forward.FAIL);
		}

	}
}
