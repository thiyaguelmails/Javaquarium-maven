package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.consts.ExceptionCode;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Classic Action
 * 
 * @author Valentin
 *
 */
public class ListerEspeceAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}
	
	/**
	 * Constant which is the default page / first page
	 */
	public static final int DEFAULT_PAGE = 1;

	/**
	 * Constant which is the number of results per page
	 */
	public static final int MAX_RESULTS_PER_PAGE = 5;

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final UserVO user = (UserVO) session.getAttribute(SessionVar.USER.toString());
		final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (user != null) {
			try {
				final int currentPage = getCurrentPageNumber(req);
				final int lastPage;
				session.setAttribute(SessionVar.TOTAL_POISSONS_SAVED.toString(),
						this.catalogueService.getAquariumPoissonService().getTotalAquariumPoisson(user.getId()));
				session.setAttribute(SessionVar.FIRST_RESULT.toString(), getFirstResult(currentPage));
				// if the aquarium is not already created
				if (session.getAttribute(SessionVar.MY_AQUARIUM.toString()) == null) {
					final Map<Integer, AquariumPoissonVO> myAquarium = this.catalogueService.getAquariumPoissonService()
							.getAllAquariumPoisson(user.getId());
					session.setAttribute(SessionVar.MY_AQUARIUM.toString(), myAquarium);
					session.setAttribute(SessionVar.TOTAL_SPECIES.toString(), myAquarium.size());
					session.setAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString(), 0);
					lastPage = getLastPageNumber(myAquarium.size());
					session.setAttribute(SessionVar.LAST_PAGE.toString(), lastPage);
				} else {
					lastPage = Integer.parseInt(session.getAttribute(SessionVar.LAST_PAGE.toString()).toString());
				}
				// if page number is valid
				if (currentPage <= lastPage && currentPage >= DEFAULT_PAGE)
					return mapping.findForward(Forward.SUCCESS);
				else
					return mapping.findForward(Forward.FAIL);
			} catch (final ServiceException se) {
				se.logError();
				messages.add(MessageType.ERROR.toString(), se.getError(null));
				saveMessages(req, messages);
				return mapping.findForward(Forward.FAIL);
			}
		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}
	}

	/**
	 * Method to get the page parameter (or a default one if errors)
	 * @param req The HttpRequest
	 * @return A page number (or a default one if empty)
	 * @throws ServiceException If the page number is not correct (String instead of Integer)
	 */
	private int getCurrentPageNumber(final HttpServletRequest req) throws ServiceException {
		try {
			final String pageNumber = req.getParameter(Param.PAGE);
			if (pageNumber == null || pageNumber.equals(""))
				return DEFAULT_PAGE;
			else
				return Integer.parseInt(pageNumber);
		}catch(NumberFormatException nfe) {
			throw new ServiceException(ExceptionCode.ERROR_INVALID_TYPE,MessageKey.ERROR_INVALID_ID);
		}
	}

	/**
	 * Method to get the last page number to know the max page number which can be displayed
	 * @param numberOfResult The total number of results (all pages)
	 * @return The total number of results
	 */
	private int getLastPageNumber(final int numberOfResult) {
		if (numberOfResult <= 0)
			return DEFAULT_PAGE;
		else
			return (int) Math.ceil((double) numberOfResult / (double) MAX_RESULTS_PER_PAGE);
	}

	/**
	 * Method to get the first result to display depending of the actualPageNumber and the number of results per page
	 * @param pageNumber The actualPageNumber to show
	 * @return The index of the first result of this page
	 */
	private int getFirstResult(final int pageNumber) {
		return (pageNumber - 1) * MAX_RESULTS_PER_PAGE;
	}

}
