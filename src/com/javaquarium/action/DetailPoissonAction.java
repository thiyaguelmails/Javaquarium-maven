package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;
import com.javaquarium.exception.ServiceException;
import com.javaquarium.util.CatalogueService;

/**
 * Classic action to get different details about a Poisson
 * 
 * @author Valentin
 *
 */
public class DetailPoissonAction extends Action {

	/**
	 * The catalogueService to instanciate any services (by Spring IoC)
	 */
	private CatalogueService catalogueService;

	/**
	 * @param catalogueService
	 *            the catalogueService to set
	 */
	public void setCatalogueService(final CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();

		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			// if parameter is present
			if (req.getParameter(Param.ESPECE_ID.toString()) != null) {
				final int especeId = Integer.parseInt(req.getParameter(Param.ESPECE_ID).toString());
				try {
					final PoissonVO poisson = this.catalogueService.getPoissonService().getOneById(especeId);
					final Map<UserVO, Integer> qtyByUser = this.catalogueService.getAquariumPoissonService()
							.getQuantityByUser(especeId);
					session.setAttribute(SessionVar.LIST_QTY_BY_USER.toString(), qtyByUser);
					session.setAttribute(SessionVar.POISSON.toString(), poisson);
				} catch (final ServiceException se) {
					se.logError();
					messages.add(MessageType.ERROR.toString(),
							se.getError(new String[] { Integer.toString(especeId) }));
					saveMessages(req, messages);
					return mapping.findForward(Forward.FAIL);
				}
			} else {
				messages.add(MessageType.ERROR.toString(), new ActionMessage(MessageKey.ERROR_MISSING_PARAMETER));
				saveMessages(req, messages);
				return mapping.findForward(Forward.FAIL);
			}
		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}

		return mapping.findForward(Forward.SUCCESS);
	}

}
