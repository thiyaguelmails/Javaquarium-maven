package com.javaquarium.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.consts.Forward;
import com.javaquarium.consts.MessageKey;
import com.javaquarium.consts.MessageType;
import com.javaquarium.consts.Param;
import com.javaquarium.consts.SessionVar;

/**
 * Action to decrease the quantity of an AquariumPoisson in the Aquarium session
 * variable
 * 
 * @author Valentin
 *
 */
public class RemoveAquariumAction extends Action {

	@Override
	@SuppressWarnings("unchecked")
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest req,
			final HttpServletResponse res) {

		final HttpSession session = req.getSession();
		final ActionMessages messages = new ActionMessages();
		
		// if the user is logged
		if (session.getAttribute(SessionVar.USER.toString()) != null) {
			final int especeId = Integer.parseInt(req.getParameter(Param.ESPECE_ID));
			final int totalNotSaved = Integer
					.parseInt(session.getAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()).toString());

			final Map<Integer, AquariumPoissonVO> myAquarium = (Map<Integer, AquariumPoissonVO>) session
					.getAttribute(SessionVar.MY_AQUARIUM.toString());

			if (myAquarium != null && myAquarium.containsKey(especeId)) {
				final AquariumPoissonVO chosen = myAquarium.get(especeId);
				// derease only possible if actual quantity > 0
				// and if quantity to decrease is not greater than actual aquantity
				if (chosen.getQuantitySaved() >= 0 && chosen.getQuantityNotSaved() > (-1) * chosen.getQuantitySaved()) {
					chosen.setQuantityNotSaved(chosen.getQuantityNotSaved() - 1);
					session.setAttribute(SessionVar.TOTAL_POISSONS_NOT_SAVED.toString(), totalNotSaved - 1);
				}
			} else {
				messages.add(MessageType.ERROR.toString(),
						new ActionMessage(MessageKey.ERROR_NOEXIST_POISSON, new String[] { Integer.toString(especeId) }));
				saveMessages(req, messages);
				return mapping.findForward(Forward.FAIL);
			}
		} else {
			messages.add(MessageType.INFO.toString(), new ActionMessage(MessageKey.WARNING_NOT_LOGGED));
			saveMessages(req, messages);
			return mapping.findForward(Forward.NOT_LOGGED);
		}
		
		// if there's a dynamic forward (to stay on the same page as listerEspece page lists multiple page of results)
		if (req.getParameter(Param.REDIRECT) != null) {
			final String redirect = req.getParameter(Param.REDIRECT);
			final ActionForward actionfor = new ActionForward();
			actionfor.setPath("/" + redirect);
			actionfor.setRedirect(true);
			return actionfor;
		}
		
		return mapping.findForward(Forward.SUCCESS);
		
	}

}
