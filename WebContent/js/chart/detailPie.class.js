/**
 * The class to define a DetailPie
 * @param pie_id Id of the pie
 * @param data_class The class which contains the data (format : username;value)
 * @returns A detailPie
 */
function detailPie(pie_id, data_class) {
    var dataset = {
            datasets: [{}],
            options: {
                responsive: true
            }
    };
    var context = $("#" + pie_id).get(0).getContext("2d");
    var size = 0;
    var total = 0;

    /**
     * Generic function to generate random numbers between intervals
     */
    function randBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /**
     * Function to define the colors used on the pie
     */
    function getRandomColors(size) {
        var randomColors = new Array();
        for (var i = 0; i < size; i++) {
            var color = "rgba(" + randBetween(0, 255) + ", " + randBetween(0, 255) + ", " + randBetween(0, 255) + ", " + "1)";
            randomColors[i] = color;
        }
        return randomColors;
    }

    /**
     * Function to fill the arrays which contains pie's data (labels and values)
     */
    function buildDataSet() {
        	var labels = new Array();
        	var values = new Array();
        	var index = 0;
        	total = 0;
        	$("." + data_class).each(function() {
        		var data = $(this).val().split(";");
        		labels[index] = data[0];
        		values[index] = data[1];
        		total += parseInt(values[index]);
        		index++;
        	});
        	dataset["datasets"][0]["backgroundColor"] = getRandomColors(values.length);
            dataset["datasets"][0]["data"] = values;
            dataset["labels"] = labels;
            size = values.length;
        
    }

    this.getPieColors = function() {
        return dataset["datasets"][0]["backgroundColor"];
    }
    
    this.setPieColors = function(colors) {
    	dataset["datasets"][0]["backgroundColor"] = colors;
    }
    
    /**
     * Number of elements/fish in the pie (0 if no data)
     */
    this.getSize = function() {
    	return size;
    }
    
    this.getTotalFish = function() {
    	return total;
    }
    
    /**
     * Build the dataset variable used to build the pie
     */
    this.buildData = function() {
    	buildDataSet();
    }

    /**
     * Show the pie with the dataset built by the function "buildDataSet"
     */
    this.showPie = function() {
        new Chart(context, {
            type: "pie",
            data: dataset
        });
    }

}