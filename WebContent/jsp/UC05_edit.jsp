<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <bean:define id="poisson" name="<%=SessionVar.VAR_TO_EDIT.toString()%>"
      type="com.javaquarium.beans.data.PoissonVO" />
   <head>
      <jsp:include page="/jsp/parts/header.jsp">
         <jsp:param name="title" value="page.title.edit_poisson" />
         <jsp:param name="title_arg" value="<%=poisson.getEspece()%>" />
      </jsp:include>
   </head>
   <body>
      <div class="container">
         <jsp:include page="/jsp/parts/locale.jsp">
            <jsp:param name="redirect"
               value="<%=Forward.GOTO_EDIT_POISSON + '?' + Param.ESPECE_ID + '=' + poisson.getCode().toString()%>" />
         </jsp:include>
         <h1>
            <bean:message key="editPoisson.form.title" arg0="<%=poisson.getEspece()%>" />
         </h1>
         <img src="./img/destiny-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <%@ include file="/jsp/parts/messages.jsp"%>
               <html:form action="/editPoisson" styleClass="form-horizontal">
                  <html:hidden name="editPoissonForm" property="code"
                     value="<%=poisson.getCode().toString()%>" />
                  <div class="form-group">
                     <label for="espece" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.name" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="espece"
                           styleClass="form-control" value="<%=poisson.getEspece()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="description" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.description" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:textarea name="editPoissonForm" property="description"
                           styleClass="form-control" value="<%=poisson.getDescription()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="couleur" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.couleur" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="couleur"
                           styleClass="form-control" value="<%=poisson.getCouleur()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="prix" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.prix" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="prix"
                           styleClass="form-control" value="<%=poisson.getPrix()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="dimension" class="control-label col-sm-4">
                        <bean:message
                           key="editPoisson.form.dimensions" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="editPoissonForm" property="dimension"
                           styleClass="form-control" value="<%=poisson.getDimension()%>" />
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                           <a class="btn btn-info btn-md"
                              href="<%=Forward.GOTO_LISTE_ESPECE%>">
                              <i
                                 class="fa fa-arrow-circle-left" aria-hidden="true"></i> 
                              <bean:message
                                 key="message.return_my_aquarium" />
                           </a>
                           <html:submit styleClass="btn btn-success col-sm-3">
                              <bean:message key="editPoisson.form.validate" />
                           </html:submit>
                           <html:reset styleClass="btn btn-warning col-sm-3">
                              <bean:message key="editPoisson.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
               </html:form>
            </div>
         </div>
      </div>
      <jsp:include page="/jsp/parts/footer.jsp" />
   </body>
</html>