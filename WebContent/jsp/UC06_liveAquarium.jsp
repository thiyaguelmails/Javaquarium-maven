<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic"
   prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <jsp:include page="/jsp/parts/header.jsp">
         <jsp:param name="title" value="page.title.live_aquarium" />
      </jsp:include>
      <link rel="stylesheet" type="text/css" href="./css/aquarium.css">
   </head>
   <body>
      <nav class="navbar navbar-inverse">
         <div class="container-fluid">
            <div class="navbar-header">
               <a class="navbar-brand" href="#">
                  <bean:message
                     key="message.aquarium_of" />
                  <bean:write
                     name="<%=SessionVar.USER.toString()%>" property="name" />
                  ( 
                  <bean:message
                     key="message.total_poissons" />
                  : 
                  <bean:write
                     name="<%=SessionVar.TOTAL_POISSONS_SAVED.toString()%>" />
                  )
               </a>
            </div>
            <ul class="nav navbar-nav">
               <li>
                  <a class="btn btn-default btn-md navbar-right navbar-link"
                     href="<%=Forward.GOTO_LISTE_ESPECE%>">
                     <bean:message
                        key="message.return_my_aquarium" />
                     <i class="fa fa-user"
                        aria-hidden="true"></i>
                  </a>
               </li>
            </ul>
         </div>
      </nav>
      <div class="bubbles">
      </div>
      <div class="ground"></div>
      <div class="rock_1"></div>
      <div class="rock_2"></div>
      <div class="rock_3"></div>
      <div class="rock_4"></div>
      <div class="rock_5"></div>
      <div class="rock_6"></div>
      <div class="rock_7"></div>
      <div class="plant_1_wrap">
         <div class="plant_1"></div>
         <div class="plant_2"></div>
         <div class="plant_3"></div>
      </div>
      <div class="plant_2_wrap">
         <div class="plant_4"></div>
         <div class="plant_5"></div>
      </div>
      <logic:iterate name="<%=SessionVar.MY_LIVE_AQUARIUM.toString()%>"
         id="poisson" indexId="index">
         <bean:define id="nomEspece" name="poisson" property="espece" />
         <div class="fish"
            id="fish_<bean:write name='index'/>_<bean:write name='nomEspece'/>">
            <img id="img-fish_<bean:write name='index'/>" data-toggle="tooltip"
               title="<bean:write name='nomEspece'/>">
         </div>
      </logic:iterate>
      <jsp:include page="/jsp/parts/footer.jsp" />
      <script src="./js/liveAquarium/bubbles.js"></script>
      <script src="./js/lib/jquery.keyframes.min.js"></script>
      <script src="./js/lib/prefixfree.min.js"></script>
      <script src="./js/liveAquarium/aquarium.js"></script>
   </body>
</html>