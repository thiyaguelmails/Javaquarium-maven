<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.action.LoginAction"%>
<%@page import="com.javaquarium.action.ListerEspeceAction"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic"
   prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- Define beans for page management -->
<bean:parameter id="current_page_number" name="<%=Param.PAGE%>" value="<%=Integer.toString(ListerEspeceAction.DEFAULT_PAGE)%>"/>
<bean:define id="last_page_number" name="<%=SessionVar.LAST_PAGE.toString()%>" type="java.lang.Integer" />
<html>
   <head>
      <jsp:include page="/jsp/parts/header.jsp">
         <jsp:param name="title" value="page.title.lister_especes" />
      </jsp:include>
   </head>
   <body>
      <div class="container">
         <jsp:include page="/jsp/parts/locale.jsp">
            <jsp:param name="redirect" value="<%=Forward.GOTO_LISTE_ESPECE%>" />
         </jsp:include>
         <h1>
            <bean:message key="message.welcome" />
            <span class="grey">
               <bean:write
                  name="<%=SessionVar.USER.toString()%>" property="name" />
            </span>
            <a class="btn btn-info btn-sm" href="<%=Forward.GOTO_MY_PROFILE%>">
               <bean:message key="login.my_profile" />
               <i class="fa fa-user"
                  aria-hidden="true"></i>
            </a>
            <a class="btn btn-danger btn-sm" href="<%=Forward.GOTO_LOGOUT%>">
               <bean:message key="login.logout" />
               <i class="fa fa-sign-out"
                  aria-hidden="true"></i>
            </a>
            <a class="btn btn-success btn-sm"
               href="<%=Forward.GOTO_LIVE_AQUARIUM%>">
               <bean:message
                  key="message.my_live_aquarium" />
               <i class="fa fa-video-camera"
                  aria-hidden="true"></i>
            </a>
         </h1>
         <!-- Message boxes for warnings/errors/info -->
         <%@ include file="/jsp/parts/messages.jsp"%>
         <div class="hud">
            <ul class="list-group">
               <li class="list-group-item bg-black">
                  <span class="bold white">
                     <bean:message key="message.my_aquarium" />
                     :
                  </span>
                  <div class="right">
                     <a class="btn btn-success btn-xs" data-toggle="confirmation"
                        data-btn-ok-label="<bean:message key="message.yes" />
                        "
                        data-btn-ok-icon="glyphicon glyphicon-check"
                        data-btn-ok-class="btn-success btn-confirmation"
                        data-btn-cancel-label="
                        <bean:message key="message.no" />
                        "
                        data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                        data-btn-cancel-class="btn-danger btn-confirmation"
                        data-title="
                        <bean:message key="message.confirmation" />
                        "
                        data-content="
                        <bean:message key="confirmation.save_aquarium" />
                        "
                        data-placement="bottom" href="<%=Forward.GOTO_SAVE_AQUARIUM%>">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> 
                        <bean:message
                           key="message.save" />
                     </a>
                     <a class="btn btn-danger btn-xs" data-toggle="confirmation"
                        data-btn-ok-label="<bean:message key="message.yes" />
                        "
                        data-btn-ok-icon="glyphicon glyphicon-check"
                        data-btn-ok-class="btn-success btn-confirmation"
                        data-btn-cancel-label="
                        <bean:message key="message.no" />
                        "
                        data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                        data-btn-cancel-class="btn-danger btn-confirmation"
                        data-title="
                        <bean:message key="message.confirmation" />
                        "
                        data-content="
                        <bean:message key="confirmation.clear_aquarium" />
                        "
                        data-placement="bottom" href="<%=Forward.GOTO_CLEAR_AQUARIUM%>">
                        <i class="fa fa-eraser" aria-hidden="true"></i> 
                        <bean:message
                           key="message.clear" />
                     </a>
                  </div>
               </li>
               <li class="list-group-item green">
                  <bean:message
                     key="message.total_poissons" />
                  <span class="badge">
                     <bean:write
                        name="<%=SessionVar.TOTAL_POISSONS_SAVED.toString()%>" />
                  </span>
               </li>
               <li class="list-group-item orange">
                  <bean:message
                     key="message.total_poissons_not_saved" />
                  <span class="badge">
                     <bean:write
                        name="<%=SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()%>" />
                  </span>
                  <!-- Blink warning triangle if actions to commit/persist -->
                  <logic:notEqual
                     name="<%=SessionVar.TOTAL_POISSONS_NOT_SAVED.toString()%>"
                     value="0">
                     <i class="fa fa-exclamation-triangle faa-flash animated red"
                        aria-hidden="true"></i>
                  </logic:notEqual>
               </li>
               <li class="list-group-item orange">
                  <a data-toggle="collapse"
                     href="#collapse-chart" class="btn btn-primary" style="display: block;">
                     <i
                        class="fa fa-arrow-circle-down fa-lg left" aria-hidden="true"></i>
                     <bean:message key="message.chart_view" />
                     <i
                        class="fa fa-arrow-circle-down fa-lg right" aria-hidden="true"></i>
                  </a>
               </li>
            </ul>
            <!-- Charts pie block -->
            <div id="collapse-chart" class="panel-collapse collapse">
               <div class="inline">
                  <div id="canvas-holder-saved"
                     class="canvas-container canvas-left left">
                     <i class="fa fa-database fa-2x" aria-hidden="true"></i>
                     <div id="canvas-container-saved">
                        <canvas id="fish_chart_saved"></canvas>
                     </div>
                  </div>
                  <div id="canvas-holder-not-saved"
                     class="canvas-container canvas-right right">
                     <i class="fa fa-desktop fa-2x" aria-hidden="true"></i>
                     <div id="canvas-container-not-saved">
                        <canvas id="fish_chart_not_saved"></canvas>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="page-number-block clear right">
            <!-- Logic for Pages numbers buttons -->
            <logic:greaterEqual name="current_page_number" value="1">
               <!-- Logic for previous page button -->
               <logic:lessEqual name="current_page_number" value="<%=Integer.toString(last_page_number)%>">
                  <logic:notEqual name="current_page_number" value="1">
                     <a class="btn btn-default btn-sm"
                        href="<%=Forward.GOTO_LISTE_ESPECE + '?' + Param.PAGE + '=' + (Integer.parseInt(current_page_number) - 1)%>">
                     <i class="fa fa-backward" aria-hidden="true"></i>
                     </a>
                  </logic:notEqual>
               </logic:lessEqual>
               <span class="bold">
                  <bean:message key="liste_poisson.page.label" />
                  <%=current_page_number%> / <%=last_page_number%>
               </span>
               <!-- Logic for next page button -->
               <logic:lessThan name="current_page_number" value="<%=Integer.toString(last_page_number)%>">
                  <a class="btn btn-default btn-sm"
                     href="<%=Forward.GOTO_LISTE_ESPECE + '?' + Param.PAGE + '=' + (Integer.parseInt(current_page_number) + 1)%>">
                  <i class="fa fa-forward" aria-hidden="true"></i>
                  </a>
               </logic:lessThan>
            </logic:greaterEqual>
         </div>
         <!-- Table with the aquarium/specie content -->
         <table class="table table-bordered" id="aquarium_user">
            <logic:greaterThan name="<%=SessionVar.TOTAL_SPECIES.toString()%>" value="0">
               <thead style="background-color: grey; color: white;">
                  <tr>
                     <th class="center"><i class="fa fa-users" aria-hidden="true"></i>
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.name" />
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.description" />
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.color" />
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.size" />
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.price" />
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.detail" />
                     </th>
                     <th>
                        <bean:message key="liste_poisson.colonne.my_aquarium" />
                     </th>
                  </tr>
               </thead>
            </logic:greaterThan>
            <tbody>
               <!-- Display only specific number of results : see length and offset attributes -->
               <logic:iterate name="<%=SessionVar.MY_AQUARIUM.toString()%>"
                  id="map" length="<%=Integer.toString(ListerEspeceAction.MAX_RESULTS_PER_PAGE)%>" offset="<%=SessionVar.FIRST_RESULT.toString()%>">
                  <bean:define id="aquariumEspece" name="map" property="value" type="com.javaquarium.beans.data.AquariumPoissonVO" />
                  <tr>
                     <th class="center" scope="row">
                        <a
                           class="btn btn-warning btn-sm"
                           href="<%=Forward.GOTO_EDIT_POISSON%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />">
                        <i class="fa fa-pencil-square" aria-hidden="true"></i>
                        </a> 
                        <a class="btn btn-danger btn-sm" data-toggle="confirmation"
                           data-btn-ok-label="<bean:message key="message.yes" />
                           "
                           data-btn-ok-icon="glyphicon glyphicon-check"
                           data-btn-ok-class="btn-success btn-confirmation"
                           data-btn-cancel-label="
                           <bean:message key="message.no" />
                           "
                           data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                           data-btn-cancel-class="btn-danger btn-confirmation"
                           data-title="
                           <bean:message key="message.confirmation" />
                           "
                           data-content="
                           <bean:message key="confirmation.remove_poisson" arg0="<%=aquariumEspece.getEspece().getEspece()%>" />
                           "
                           data-placement="bottom"
                           href="<%=Forward.GOTO_REMOVE_POISSON%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />">
                           <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                     </th>
                     <th scope="row" class="fish_name">
                        <bean:write
                           name='aquariumEspece' property='espece.espece' />
                     </th>
                     <th scope="row">
                        <bean:write name='aquariumEspece'
                           property='espece.description' />
                     </th>
                     <th scope="row">
                        <bean:write name='aquariumEspece'
                           property='espece.couleur' />
                     </th>
                     <th scope="row">
                        <bean:write name='aquariumEspece'
                           property='espece.dimension' />
                     </th>
                     <th scope="row">
                        <bean:write name='aquariumEspece'
                           property='espece.prix' />
                     </th>
                     <th scope="row">
                        <a class="btn btn-primary btn-sm"
                           href="<%=Forward.GOTO_DETAIL_POISSON%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />">
                           <bean:message key="liste_poisson.colonne.detail" />
                           <i
                              class="fa fa-info-circle" aria-hidden="true"></i>
                        </a>
                     </th>
                     <th scope="row">
                        <a class="btn btn-success btn-sm"
                           href="<%=Forward.GOTO_AJOUT_AQUARIUM%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />&<%=Param.REDIRECT%>=<%=Forward.GOTO_LISTE_ESPECE%>?<%=Param.PAGE%>=<%=current_page_number%>">
                           <bean:message key="liste_poisson.my_aquarium.add" />
                           <i
                              class="fa fa-plus-circle" aria-hidden="true"></i>
                        </a>
                        <a class="btn btn-danger btn-sm"
                           href="<%=Forward.GOTO_ENLEVE_AQUARIUM%>?<%=Param.ESPECE_ID%>=<bean:write name='aquariumEspece' property='espece.code' />&<%=Param.REDIRECT%>=<%=Forward.GOTO_LISTE_ESPECE%>?<%=Param.PAGE%>=<%=current_page_number%>">
                           <bean:message key="liste_poisson.my_aquarium.remove" />
                           <i
                              class="fa fa-minus-circle" aria-hidden="true"></i>
                        </a>
                        <div class="right">
                           <bean:message key="liste_poisson.my_aquarium.actual" />
                           : 
                           <span class="green fish_qty_saved">
                              <bean:write
                                 name='aquariumEspece' property='quantitySaved' />
                           </span>
                           <i class="fa fa-database grey"></i> <i class="fa fa-exchange"></i>
                           <span class="red fish_qty_not_saved">
                              <bean:write
                                 name='aquariumEspece' property='quantityNotSaved' />
                           </span>
                           <i class="fa fa-exclamation-triangle orange"></i>
                        </div>
                     </th>
                  </tr>
               </logic:iterate>
            </tbody>
         </table>
         <a class="btn btn-info" href="<%=Forward.GOTO_AJOUT%>">
            <bean:message
               key="liste_poisson.ajouter_poisson" />
            <i class="fa fa-plus"
               aria-hidden="true"></i>
         </a>
         <img src="./img/dory-icon.png" class="right" /> <br>
      </div>
      <!-- Store all aquarium data for charts (can't use the table data because partial results /incomplete) -->
      <logic:iterate name="<%=SessionVar.MY_AQUARIUM.toString()%>"
         id="map">
         <bean:define id="aquaPoisson" name="map" property="value"
            type="com.javaquarium.beans.data.AquariumPoissonVO" />
         <html:hidden name="" property="data"
            value="<%=aquaPoisson.getEspece().getEspece() + ';' + aquaPoisson.getQuantitySaved() + ';' + aquaPoisson.getQuantityNotSaved()%>"
            styleClass="chart-data" />
      </logic:iterate>
      <jsp:include page="/jsp/parts/footer.jsp" />
      <script src="./js/lib/Chart.min.js"></script>
      <script src="./js/chart/aquariumPie.class.js"></script>
      <script src="./js/chart/chartAquarium.js"></script>
   </body>
</html>