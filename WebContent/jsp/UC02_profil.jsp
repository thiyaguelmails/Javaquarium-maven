<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <bean:define id="user" name="<%=SessionVar.USER.toString()%>" type="com.javaquarium.beans.data.UserVO" />
   <head>
      <jsp:include page="/jsp/parts/header.jsp">
         <jsp:param name="title" value="page.title.user_profile" />
      </jsp:include>
   </head>
   <body>
      <div class="container">
         <!-- Locale switcher with flags -->
         <jsp:include page="/jsp/parts/locale.jsp">
            <jsp:param name="redirect" value="<%=Forward.GOTO_MY_PROFILE%>" />
         </jsp:include>
         <h1>
            <bean:message key="editProfile.form.title" arg0="<%=user.getUsername()%>" />
         </h1>
         <img src="./img/turtle-icon.png" class="right" />
         <div class="row">
            <div class="col-xs-7">
               <!-- Message boxes for warnings/errors/info -->
               <%@ include file="/jsp/parts/messages.jsp"%>
               <!-- Beginning Form -->
               <html:form action="/editProfile" focus="name"
                  styleClass="form-horizontal">
                  <html:hidden name="profileForm" property="userId"
                     value="<%=Integer.toString(user.getId())%>" />
                  <html:hidden name="profileForm" property="username"
                     value="<%=user.getUsername()%>" />
                  <!-- User name -->
                  <div class="form-group">
                     <label for="username" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.username" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <input type="text" class="form-control" id="usr"
                           value="<%=user.getUsername()%>" disabled>
                     </div>
                  </div>
                  <!-- Name -->
                  <div class="form-group">
                     <label for="name" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.name" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:text name="profileForm" property="name"
                           styleClass="form-control" value="<%=user.getName()%>" />
                     </div>
                  </div>
                  <!-- Change Password ? -->
                  <div class="form-group">
                     <label for="changePassword" class="control-label col-sm-4">
                        <bean:message key="editProfile.form.change_password" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:checkbox name="profileForm" property="changePassword"
                           styleClass="checkbox" />
                     </div>
                  </div>
                  <!-- Password -->
                  <div class="form-group">
                     <label for="password" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.new_password" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="profileForm" property="newPassword"
                           styleClass="form-control switch" />
                     </div>
                  </div>
                  <!-- Password again -->
                  <div class="form-group">
                     <label for="password2" class="control-label col-sm-4">
                        <bean:message
                           key="editProfile.form.new_password2" />
                        :
                     </label>
                     <div class="col-sm-7">
                        <html:password name="profileForm" property="newPassword2"
                           styleClass="form-control switch" />
                     </div>
                  </div>
                  <!-- Buttons -->
                  <div class="form-group">
                     <div class="col-xs-offset-2">
                        <div class="btn-toolbar">
                           <a class="btn btn-info btn-md"
                              href="<%=Forward.GOTO_LISTE_ESPECE%>">
                              <bean:message
                                 key="message.return" />
                           </a>
                           <html:submit styleClass="btn btn-success col-sm-3">
                              <bean:message key="editProfile.form.validate" />
                           </html:submit>
                           <html:reset styleClass="btn btn-warning col-sm-3">
                              <bean:message key="editProfile.form.reset" />
                           </html:reset>
                        </div>
                     </div>
                  </div>
                  <!-- End form -->
               </html:form>
            </div>
         </div>
      </div>
      <!-- JS files -->
      <jsp:include page="/jsp/parts/footer.jsp" />
   </body>
</html>